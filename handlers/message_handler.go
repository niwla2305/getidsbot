package handlers

import (
	"fmt"
	"strconv"

	tb "gopkg.in/tucnak/telebot.v2"
)

func bool_to_emoji(value bool) string {
	emoji := "❌"
	if value == true {
		emoji = "✅"
	}
	return emoji
}

func (bc BotContext) MessageHandler(m *tb.Message) {
	messageid := m.ID
	userid := m.Sender.ID
	chatid := m.Chat.ID
	forwarded := m.IsForwarded()

	text := fmt.Sprintf(`
	Information about the message you just sent:
	- 4️⃣ ID in this chat: *%d*
	- ⛱ Your UserID: *%d*
	- ➡️ Forwarded: *%s*
	- 💭 Chat ID: *%d*
	`, messageid, userid, bool_to_emoji(forwarded), chatid)
	if forwarded {
		originialsender := m.OriginalSender
		var originialchatid string
		if m.FromChannel() {
			originialchatid = strconv.FormatInt(m.OriginalChat.ID, 10)
		} else {
			originialchatid = "Not found"
		}
		fmt.Println(originialchatid)
		originalsenderid := originialsender.ID
		originalmessageid := m.OriginalMessageID
		originalsendername := fmt.Sprintf("%s %s (@%s)", originialsender.FirstName, originialsender.LastName, originialsender.Username)
		text += fmt.Sprintf(`		- 👩‍💻 Senders UserID: *%d*
			- 📦 Original Message ID: *%d*
			- 👩‍🦰 Original Author Name: *%s*
			- 🚦 ID of originial Chat: *%s*
			`, originalsenderid, originalmessageid, originalsendername, originialchatid)
	}

	bc.Bot.Send(m.Sender, text, &tb.SendOptions{
		ParseMode: tb.ModeMarkdown,
	})
}
