package handlers

import (
	"fmt"

	tb "gopkg.in/tucnak/telebot.v2"
)

func (bc BotContext) ChannelPostHandler(m *tb.Message) {

	messageid := m.ID
	chatid := m.Chat.ID

	text := fmt.Sprintf(`
- 🆔 Message ID: *%d*
- 💬 Chat ID of this Channel: *%d*
	`, messageid, chatid)

	bc.Bot.Send(m.Chat, text, &tb.SendOptions{
		ParseMode: tb.ModeMarkdown,
	})
}
