# getidsbot

A telegram bot to retreive data like chatids and userids written in Go

## Installation

### Docker-Compose

* Clone this repository
* `cp .env.example .env`
* Change the Token in .env to yours you got from BotFather
* Run `docker-compose up -d`

### Linux

 * 

``` bash
    $ go get -u {{gopackage}}
    $ go install {{gopackage}}
    ```

 * Binary is now available in `$GOPATH/bin` .

## Contribution

Contribution to this project is welcome. For smaller changes just make a PR. If it is a bigger thing please create an issue first and get 
approval from niwla23 for the idea. If you contribute to the project you agree that your contributions are released
under the GNU Affero General Public License (AGPL)
