package main

import (
	"log"
	"os"
	"time"

	"gitlab.com/niwla23/getidsbot/handlers"
	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	if os.Getenv("TOKEN") == "" {
		log.Fatal("Please provide bot token via WA2TG_TOKEN env var")
		return
	}

	b, err := tb.NewBot(tb.Settings{
		Token:  os.Getenv("TOKEN"),
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	bc := handlers.BotContext{
		Bot: b,
	}

	b.Handle(tb.OnText, bc.MessageHandler)
	b.Handle(tb.OnChannelPost, bc.ChannelPostHandler)

	// Command handlers
	b.Handle("/start", bc.MessageHandler)
	b.Handle("/license", bc.MessageHandler)

	b.Start()
}
